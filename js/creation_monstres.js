// Modèle de base pour la création des monstres
var monster = {

  decrire: function() {
    var description = this.pseudo + " est un digimon de type " + this.typo + " de niveau " + this.level + " et a " +
    this.attack + " points d'attaque, " + this.defend + " en défense et " + this.special + " d'attaque spéciale.";
    return description;
  },

  newmonster: function(pseudo,level,typo,attribute,family,weight,prior_form,next_form,attack,defend,special) {
    this.pseudo = pseudo;
    this.level = level;
    this.typo = typo;
    this.attribute = attribute;
    this.family = family;
    this.attack = attack;
    this.defend = defend;
    this.special = special;

  }
};

// Créer des digimons avec monstre comme prototype
var agumon = Object.create(monster);
agumon.newmonster('Agumon','Rookie','Reptile','Vaccine','Nature Spirits', 140, 230, 380);

var aldamon = Object.create(monster);
aldamon.newmonster('Aldamon','Warrior','Dragon','Hybrid','Demon Man', 470, 340, 680);

// Afficher les caractéristiques
document.write(agumon.decrire()+'<br>');
document.write(aldamon.decrire()+'<br>');
